@extends('layouts')

@section('content')
<section class="container">
	<h1 class="title">Mon panier</h1>
	<hr>
	<table class="table is-striped is-narrow is-hoverable is-fullwidth">
		<thead>
			<tr>
				<th>Articles</th>
				<th>Prix</th>
				<th>Quantité</th>
			</tr>
		</thead>
			<tbody>
					{{-- Blade : Boucles pour récupérer des produits commander ( https://laravel.com/docs/5.8/blade), 
							 Bulma : https://bulma.io/documentation/elements/table/--}}
			</tbody>
			<tfoot>
				<tr>
					<th>Articles</th>
					<th>Prix</th>
					<th>Quantité</th>
				
					<tbody>
						@foreach ($cart as $item)
							<tr>
								<td>{{$item[1]}}</td> <!--NAME-->
								<td>{{$item[2]}}€</td> <!--PRIX-->
								<td>{{$item[3]}}</td> <!--QUANTITY-->
								{{-- <td>{{$item[4]}}</td> <!--QUANTITY--> --}}

							</tr>
						@endforeach	
   					 </tbody>		
				<th>
					{{-- Afficher le prix totals de tout les produits --}}
					
						
					
					<h3 class="subtitle is-5">Total(€): {{$total}} € </h3>
					
					{{-- afficher le nombre de produits aux totals --}}
				<h3 class="subtitle is-5">Nombre de produits : {{$count}}</h3>
					<div class="buttons">
						<a href="/" class="button is-small is-default">Retour au shopping</a>
						<a href="/order" class="button is-small is-success">Valider la commande</a>
					</div>
				</th>
			</tr>
		</tfoot>
	</table>
</section>
@endsection