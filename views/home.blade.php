@extends('layouts')

@section('content')
	<section class="container">	
		<h1 class="title">Listes des produits</h1>
		<hr>
		<div class="columns">
			{{-- Boucles pour récupérer des produits (https://laravel.com/docs/5.8/blade), 
					 Bulma : https://bulma.io/documentation/columns/responsiveness/,
									 https://bulma.io/documentation/components/card/
			 --}}
			@foreach($products as $product)
				<div class="card">
  					<div class="card-image">
    					<figure class="image is-4by3">
							<a href="product/{{$product->id}}">
      						<img src="{{$product->picture}}" alt="Placeholder image">
							 </a> 
    					</figure>
  					</div>
 					<div class="card-content">
    					<div class="media">
      					<div class="media-content">
       						 <p  class="title is-4">{{$product->name}}</p>
       						 <p class="subtitle is-6">{{$product->price}}€</p>
								
      					</div>
    				</div>
				</div>
			@endforeach
		</div>
	</section>
@endsection