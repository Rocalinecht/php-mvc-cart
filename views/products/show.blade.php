@extends('layouts')

@section('content')
	<section class="container">

		<p class="title is-4">{{$the_products->name}}</p>
		<img src="{{$the_products->picture}}" alt="picture">
		<p>{{$the_products->price}}€</p>
		<div class="buttons">
			<form action="/cart/add" method='POST'>
				<input type="hidden" name="name" value="{{$the_products->name}}" />
                <input type="hidden" name="price" value="{{$the_products->price}}" />
                <input type="hidden" name="id" value="{{$the_products->id}} " />
                <input type="number" name="quantity" value="1" min="0" />
                <button class="button is-small is-default">Ajouter au panier</button>
            </form>
						
		</div>
		
	</section>
@endsection
