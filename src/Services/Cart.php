<?php

namespace App\Services;

class Cart
{
	/**
	 * Retourne le tableau produits ajouter par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}
	public static function add($request){//---------------------> AJOUTE LES PORDUIT AU PANIER 

		//____> stocker info input dans la Session
		$name = $request->input('name');
		$price = $request->input('price');
		$quantity = $request->input('quantity');
		$id = $request->input('id');
		$prodCart = [$id,$name, $price,$quantity];
		
		//____> Demander a Session de creer un tableau pour enregister info
		if(!isset($_SESSION['cart'])){
			$_SESSION['cart'] =[];
		}
		//_____> Si produit exist, augmenter seulment quantité 
		if(isset($_SESSION['cart'][$id])){

			$_SESSION['cart'][$id][3] += $quantity;
		}
		else{

			$_SESSION['cart'][$id] = $prodCart;
		}
	}	
	
	public function count($request){//-----------------------------> COMPTE LE NOMBRE DE PORDUIT DANS PANIER 
	
		$nbrProduit = 0;

	 	foreach($_SESSION['cart'] as $nbrProd){
		$nbrProduit += $nbrProd[3];
		}
		// return $nbrProduit;
		return $nbrProduit;
	
	
	}
	public function total($request){ //-------------------->CALCUL LE PRIX TOTAL DU PANIER
		
		//[2] -> prix | [3]-> quantity
		//__> boucler le tableau & multitplier le prix par la quantité
		foreach($_SESSION[cart] as $p){

				$price = $p[2] * $p[3];
				$priceTotal = $priceTotal + $price;
		}
		return $priceTotal;
	}
}